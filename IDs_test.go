package uniqueID

import (
	"testing"

	tools "gitlab.com/Pixdigit/GoTestTools"
)

func TestBaseIDSpace(t *testing.T) {
	ID1 := NewID()
	ID2 := NewID()
	ID3 := NewID()
	tools.Test(ID1 != ID2 && ID2 != ID3 && ID3 != ID1, "duplicate IDS", t)
	tools.Test(IDExists(ID3), "did not confirm existance of ID", t)
	tools.Test(!IDExists(ID(5)), "wrongly confirmed existance of ID", t)
}
