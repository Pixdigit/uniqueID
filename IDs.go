package uniqueID

type ID int

type IDSpace struct {
	freeID ID
}

func (s *IDSpace) NewID() ID {
	newID := s.freeID
	s.freeID++
	return newID
}

func (s *IDSpace) IDExists(id ID) bool {
	//All IDs below freeID are taken and therefore exist
	return s.freeID > id
}
