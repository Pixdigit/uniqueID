package uniqueID

var baseSpace IDSpace

func NewID() ID {
	return baseSpace.NewID()
}
func IDExists(id ID) bool {
	return baseSpace.IDExists(id)
}
