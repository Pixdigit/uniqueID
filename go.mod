module gitlab.com/Pixdigit/uniqueID

go 1.13

require (
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/Pixdigit/GoTestTools v0.0.0-20180720152516-ad30e7e0c720
)
